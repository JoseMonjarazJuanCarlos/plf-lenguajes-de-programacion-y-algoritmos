# Lenguajes de programación y algoritmos
## Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)

```plantuml
@startwbs
skinparam backgroundColor #FFFFFF
*[#044E51] <font color=#FFFFFF><b>Programando ordenadores en los 80 y ahora

**[#079CA1] <font color=#FFFFFF><b>Programar
***_ <font color=#044E51><b>definición
**** Dar las instrucciones necesarias a una máquina para que realice alguna función.
***[#36C5FF]< <font color=#FFFFFF><b>Antes
****_< <font color=#044E51><b>características
*****< Se necesitaba \nconocer la \narquitectura de \ncada máquina
*****> Se programaba \nen ensamblador
****_ <font color=#044E51><b>ventajas 
*****< Aprovecha mejor \nlos recursos de \la máquina
*****> Primaba la \neficiencia \ndel programa
****_> <font color=#044E51><b>desventajas 
*****< Las restricciones \nde hardware \ndefinían el \nlenguaje 
*****> Cuellos de botella \npor el hardware
***[#36C5FF]> <font color=#FFFFFF><b>Ahora
****_< <font color=#044E51><b>ventajas
*****< Puedes optar por lenguajes de programación
*****> Obtenemos programas más entendibles
*****< Se puede ejecutar el código en maquinas distintas 
*****> Software más complejo
****_> <font color=#044E51><b>desventajas 
*****< El producto se presenta parcial
*****> Sobrecarga de capas

**[#079CA1] <font color=#FFFFFF><b>Sistemas
***_ <font color=#044E51><b>existen los 
****[#36C5FF]< <font color=#FFFFFF><b>Antiguos
*****_< <font color=#044E51><b>es 
******< Aquel que no \nestá en venta \no no tuvo \ncontinuidad
*****_> <font color=#044E51><b>su velocidad  
****** Se media en \nmegahercio \n(MHz)
****[#36C5FF]> <font color=#FFFFFF><b>Actuales
*****_< <font color=#044E51><b>es 
******< Aquel que se \nencuentra vigente \ny en venta
*****_> <font color=#044E51><b>su velocidad  
****** Se mide en \ngigahercio \n(GHz)

**[#079CA1] <font color=#FFFFFF><b>Cambios
***[#36C5FF]< <font color=#FFFFFF><b>Antes
****< La complejidad \nestaba en \nel código útil
****< Eran iguales en \npotencia, pero \ncon aspecto diferente 
***[#36C5FF]> <font color=#FFFFFF><b>Ahora
****> La complejidad \nestá en la \nsecuencia de llamadas
****> Son diferentes \nen potencia y \nsimilares en aspecto
@endtwbs
```

[Referencia video 1](https://canal.uned.es/video/5a6f4bdcb1111f082a8b4619)

## Hª de los algoritmos y de los lenguajes de programación (2010)

```plantuml
@startwbs
skinparam backgroundColor #FFFFFF
*[#044E51] <font color=#FFFFFF><b>Historia de los algoritmos y de los lenguajes de programación

**[#079CA1] <font color=#FFFFFF><b>Algoritmo
***[#36C5FF]< <font color=#FFFFFF><b>Definición
****_< <font color=#044E51><b>es un
*****< Procedimiento sistemático y \nmecánico para resolver un problema
******[#F95A0B]< Funcionamiento
*******< Reciben una \nentrada y la \ntransforman \nen una salida 
********_ <font color=#044E51><b>se
*********< Comporta como \nuna caja negra
******[#F95A0B]> Se clasifica en
*******_< <font color=#044E51><b>razonables 
********< Su tiempo de \nejecución crece despacio 
*********_ <font color=#044E51><b>son llamados
**********< Polinomiales 
*******_> <font color=#044E51><b>no razonables 
********> Crecen demasiado \nrápido
*********_> <font color=#044E51><b>conocidos como 
**********< Exponenciales
**********> Super polinomiales 
***[#36C5FF]> <font color=#FFFFFF><b>Antecedentes
****[#F95A0B]< Mesopotamia
***** Se emplearon para \ndescribir ciertos \ncálculos
****[#F95A0B]> Siglo XVII 
***** Caculadoras mecánicas
****[#F95A0B]> Siglo XIX 
***** Primeras máquinas \nprogramables
****[#F95A0B]< Siglo XX 
***** Ordenadores parecidos \na los actuales
***[#36C5FF]> <font color=#FFFFFF><b>Características
****< Son más antiguos \nque los ordenadores
****> Son empleados en \nla vida cotidiana
****< Orientado a la \nresolución de problemas
****> Funciona como \nuna caja negra 

**[#079CA1] <font color=#FFFFFF><b>Lenguaje de programación
***[#36C5FF]< <font color=#FFFFFF><b>Definición
****_ <font color=#044E51><b>es
***** Un lenguaje formal
******_< <font color=#044E51><b>permite
*******< Escribir un conjunto de órdenes
******_> <font color=#044E51><b>Sirve
*******> Para crear programas \nque controlen el \ncomportamiento físico y \nlógico de una máquina.
******_< <font color=#044E51><b>populares
*******< C++
*******> C
*******< Java
***[#36C5FF] <font color=#FFFFFF><b>Paradigmas
****_ <font color=#044E51><b>Se clasifica en 
*****[#F95A0B]< Imperativos
******_< <font color=#044E51><b>pionero
*******< FORTRAN 1957
*****[#F95A0B]< Orientados a objetos
******_< <font color=#044E51><b>pionero
*******< Simula 1968
*****[#F95A0B]> Declarativos
******_ <font color=#044E51><b>Se dividen en
*******< Funcionales
********_ <font color=#044E51><b>pionero
*********< PL/I 1963
*******> Lógicos
********_ <font color=#044E51><b>pionero
*********> PROLOG 1971
@endtwbs
```

[Referencia video 2](https://canal.uned.es/video/5a6f4c8fb1111f082a8b4b9c)

## Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

```plantuml
@startwbs
skinparam backgroundColor #FFFFFF
*[#044E51] <font color=#FFFFFF><b>Evolución de los lenguajes y paradigmas de programación

**[#079CA1] <font color=#FFFFFF><b>Lenguaje de programación
***_< <font color=#044E51><b>surge
**** De la necesidad \nde indicar a la \nmáquina que hacer
***_< <font color=#044E51><b>debe
**** Ser adaptado a \nlas necesidades
***_< <font color=#044E51><b>tiene que
**** Ofrecer eficiencia \nen ejecución
***_> <font color=#044E51><b>antes
**** Se utilizaban acrónimos
*****_> para
****** Asociar a códigos numéricos  
*****_> se utiliza
******< Programación de \nperiféricos 
******> Cuando se busca \n eficiencia

**[#079CA1] <font color=#FFFFFF><b>Paradigma
***_ <font color=#044E51><b>es
**** Una forma de pensar o abordar un problema
*****_< <font color=#044E51><b>se tiene
******[#36C5FF]< <font color=#FFFFFF><b>Programación estructurada
*******_< <font color=#044E51><b>orientado a
******** Mejorar la claridad, calidad \ny tiempo de desarrollo  
*******_> <font color=#044E51><b>ofrece 
******** una concepción al diseño del \nsoftware más modular
******[#36C5FF]> <font color=#FFFFFF><b>Funcional 
*******_< <font color=#044E51><b>se basa  
******** En el uso del lenguaje\n de las matemáticas
*******_> <font color=#044E51><b>no tiene 
******** Secuencias de control
******[#36C5FF]< <font color=#FFFFFF><b>Lógico
*******_< <font color=#044E51><b>se basa  
******** En expresiones lógicas
*******_> <font color=#044E51><b>tiene 
******** Predicados y sentencias lógicas
******[#36C5FF]> <font color=#FFFFFF><b>Concurrente
*******_< <font color=#044E51><b>se tiene 
******** Mutiplicidad de conexiones
*******_> se deben
******** Establecer políticas
******[#36C5FF]< <font color=#FFFFFF><b>Orientado a objetos
*******_< <font color=#044E51><b>se basa 
******** En el imperativo
*******_> encapsula
******** Elementos denominados objetos
@endtwbs
```

[Referencia video 3](https://canal.uned.es/video/5a6f4d3cb1111f082a8b4fbc)